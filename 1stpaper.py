#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 17:26:57 2019

"""
import numpy as np
import pylab
from scipy.optimize import curve_fit

# read in public auger data
#ids, n_stations, zeniths, azimuths, energies, timestamps, gal_lons, gal_lats = np.loadtxt("/home/Oz/LIP/Auger/auger_public.txt", unpack=True)

ids, n_stations, zeniths, azimuths, energies, timestamps, gal_lons, gal_lats,ra,dec,deltas = np.loadtxt("/home/Oz/LIP/Auger/deltaevents.txt", unpack=True)

log_energies = np.log10(energies * 1.e18)  # calculate logarithm of energy
dyp = 0.974*log_energies-18.08
dyf = 1.06*log_energies-20.47
energies=energies[(deltas>dyp)]
log_energies = np.log10(energies * 1.e18) 

#Guardar altura do histograma e range dos bins
N=30 #nº de bins
hg=pylab.hist(log_energies, bins=N, range=(17.5, 20.5), histtype='stepfilled') #guarda um vetor de vetores hg.

hval=hg[0]  #Guarda a altura do histograma

edges=10**hg[1] #Guarda os limites dos bins

avgEn=edges[0:N]+(edges[1:(N+1)]-edges[0:N])/2 #equivalente:avgEn=edges[1:(N+1)]+edges[0:N])/2: faz um vetor com os valores medios de energia de cada bin
avgLogEn=np.log10(avgEn) #faz os logaritmos de cada valor medio: tambem faz um vetor com os valores

exposure=57583.57*0.1 #por so termos 10% dos dados
norm=edges[1:(N+1)]-edges[0:N] #da o valor da largura de cada bin

#f=pylab.figure() #cria a figura para o grafico 
#ax=pylab.subplot(111) #localizar o grafico na figura
avgLogHE=avgLogEn[(avgLogEn>18.47)*(avgLogEn<20.2)] #vai nos so buscar os valores do vetor com logaritmos superiores a 18,47
avgHE=avgEn[(avgLogEn>18.47)*(avgLogEn<20.2)]
fluxHE=0.9*((hval*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/exposure #da nos o valor do fluxo normalizado!!!(em parte: falta a exposiçao)
#pylab.yscale('log') #faz a escala logaritmica
#nomes dos eixos

#pylab.ylim(10**36,10**38)

error=0.9*((np.sqrt(hval)*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/exposure
#desenha o grafico

j0=2.99179297e-19
def fit(E,Es, γ1, γ2, Δγ):
    return j0*(E**3)*np.piecewise(E,
                        [E<5.08e18, E>=5.08e18],
                        [lambda E: (E/5.08e18)**(-γ1),
                         lambda E: ((E/5.08e18)**(-γ2))*(1+((5.08e18/Es)**Δγ))*(1+(E/Es)**Δγ)**(-1) ])



#params, pcov=curve_fit(fit,avgHE, fluxHE, [ 3.9e+19,3.293, 2.53, 2.5], sigma=error, absolute_sigma=1)
#fitpoints=fit(avgHE,params[0],params[1],params[2],params[3])



pylab.figure()
#pylab.plot(avgLogHE,fitpoints, lw=0.5)
pylab.errorbar(avgLogHE, fluxHE,yerr=error,fmt='or',capsize=0, ms=3, elinewidth=1, ecolor='r', mew=1, mec='k', mfc='k')
pylab.yscale('log')
pylab.ylim(1e35,1e38)
pylab.xlabel('log(E)') 
pylab.ylabel('Flux')

pylab.savefig('paper1.png', dpi=1000)