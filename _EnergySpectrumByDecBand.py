#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Criado por Pedro Branco, Osvaldo Freitas e Pedro Passos no ambito dos estagios de verao do LIP-Minho 2019.
Projeto orientado por Raul Sarmento
"""



import numpy as np
import pylab

# read in public auger data
ids, n_stations, zeniths, azimuths, energies, timestamps, gal_lons, gal_lats = np.loadtxt("auger_public.txt", unpack=True)
ra,dec=ga2equ([gal_lons,gal_lats]) #Calcular coordenadas equatoriais
N=30 #nº de bins a usar nos histogramas

########################################################################################################
#%%Espectro total
log_energies = np.log10(energies * 1.e18)  # calculate logarithm of energy
(hval,logedges)=np.histogram(log_energies, bins=N, range=(17.5, 20.5) ) #guarda a altura do histograma e os limites dos bins

#Os vetores neste bloco so precisam de ser calculados uma vez; 
edges=10**logedges                                  #Guarda os limites dos bins
avgEn=(edges[1:(N+1)]+edges[0:N])/2                 #Cria um vetor com os valores medios de energia de cada bin
avgLogEn=np.log10(avgEn)                            #Calcula os logaritmos de cada valor medio
avgLogHE=avgLogEn[(avgLogEn>18.47)*(avgLogEn<20.2)] #Faz corte para bins de alta energia
avgHE=avgEn[(avgLogEn>18.47)*(avgLogEn<20.2)]       #Mesmo que o anterior
exposure=57583.57*0.1                               #Valor de exposiçao obtida pela calculadora disponibilizada pelo observatorio. Fator de 0.1 devido a tratarmos apenas 10% dos dados
norm=edges[1:(N+1)]-edges[0:N]                      #Calcula o valor da largura de cada bin
########


fluxHE=0.9*((hval*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/(exposure) #Calcula o valor do fluxo normalizado a exposure e largura de bins
error=0.9*((np.sqrt(hval)*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/exposure


########################################################################################################
#%%Banda -90.0º<δ<-15.7º

cut1=(dec>-90)*(dec<-15.7)

log_energies_cut1 = np.log10(energies * 1.e18)[cut1]  # calculate logarithm of energy
(hval_cut1,_)=np.histogram(log_energies_cut1, bins=N, range=(17.5, 20.5) ) #guarda um vetor de vetores hg.

decWeight1=0.68
fluxHE_cut1=0.9*((hval_cut1*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/(exposure*decWeight1) #Normalizar fluxo tendo em conda exposure na banda de declinaçao
error_cut1=0.9*((np.sqrt(hval_cut1)*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/(exposure*decWeight1)


#########################################################################################################
#%%Banda -15.7º<δ<25.0º

cut2=(dec>-15.7)*(dec<25.0)

log_energies_cut2 = np.log10(energies * 1.e18)[cut2]  # calculate logarithm of energy
(hval_cut2,_)=np.histogram(log_energies_cut2, bins=N, range=(17.5, 20.5) ) #guarda um vetor de vetores hg.

decWeight2=0.32
fluxHE_cut2=0.9*((hval_cut2*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/(exposure*decWeight2) #Normalizar fluxo tendo em conda exposure na banda de declinaçao
error_cut2=0.9*((np.sqrt(hval_cut2)*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/(exposure*decWeight2)


#########################################################################################################
#%%Plotting

pylab.figure()
pylab.errorbar(avgLogHE, fluxHE,yerr=error,fmt='ok',capsize=0, ms=3, elinewidth=1, ecolor='k', mew=1, mec='k',label='SD-1500 vertical', mfc='k')
pylab.errorbar(avgLogHE, fluxHE_cut1,yerr=error,fmt='sb',capsize=0, ms=3, elinewidth=1, ecolor='b', mew=1, mec='b',label='-90º<δ<-15.7º', mfc='b')
pylab.errorbar(avgLogHE, fluxHE_cut2,yerr=error_cut2,fmt='^r',capsize=0, ms=3, elinewidth=1, ecolor='r', mew=1, mec='r',label='-15.7º<δ<25.0º', mfc='r')

pylab.yscale('log')
pylab.xlim(18.5,20.25)
pylab.ylim(1e36,1e38)
pylab.xlabel('log(E)') 
pylab.ylabel('Flux')
pylab.legend()

#pylab.savefig('paper1_decwindows.png', dpi=1000)