#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Criado por Pedro Branco, Osvaldo Freitas e Pedro Passos no ambito dos estagios de verao do LIP-Minho 2019.
Projeto orientado por Raul Sarmento
"""



import numpy as np
import pylab

ids, n_stations, zeniths, azimuths, energies, timestamps, gal_lons, gal_lats, ra, dec, deltas = np.loadtxt("deltaevents.txt", unpack=True)
N=20
#del hg
#del hgw
pylab.figure()
pylab.title('Histograma do nº de eventos \npor bin de logaritmo de energia')
log_energies = np.log10(np.array(energies) * 1.e18)  # calculate logarithm of energy
(hval,logedges,_)=pylab.hist(log_energies, bins=N, range=(18.5, 20.5), histtype='stepfilled')
edges=10**logedges

avgEn=(edges[1:(N+1)]+edges[0:N])/2 
avgLogEn=np.log10(avgEn)
norm=edges[1:(N+1)]-edges[0:N]

pylab.figure()
pylab.title('Histograma do nº de eventos pesados \npelo valor de Δs respetivo')
(hvalweighted,_,_)=pylab.hist(log_energies, bins=N, weights=deltas, range=(18.5, 20.5), histtype='stepfilled')

pylab.figure()
pylab.title('Valor dos bins do histograma pesado\n dividido por nº de eventos no bin')
avgΔs=hvalweighted[avgLogEn>18.47]/hval[avgLogEn>18.47]
(a,b)=np.polyfit(avgLogEn[avgLogEn>18.47][:-2], avgΔs[:-2]/avgΔs[4]-1, 1)

#pylab.plot(log_energies,deltas, 'o', ms=1)
pylab.plot(avgLogEn[avgLogEn>18.47], (avgΔs), 'sk')


x_d = np.linspace(18.4, 22.2, 2)
yp = 1*x_d-18.39
dyp = 0.974*x_d-18.08
yf=1.06*x_d-20.47
dyf=0.915*x_d-18.09
#pylab.plot(xs, a*xs+b,'-r')


pylab.plot(x_d, yp,'-r')
pylab.plot(x_d, dyp,'--r')
pylab.plot(x_d, yf,'-b')
pylab.plot(x_d, dyf,'--b')


pylab.ylim(-1.5,2)
pylab.xlim(18.5,20.25)