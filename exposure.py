#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 16:18:51 2019

@author: Oz
"""
import numpy as np
import pylab

xdec, y=np.loadtxt('/home/Oz/LIP/Auger/ExpDec.txt',unpack=1 )

AreaTot= np.trapz(y,xdec)

AreaBlu=np.trapz(y[xdec<-15.7],xdec[xdec<=-15.7])

AreaRed=np.trapz(y[xdec>-15.7],xdec[xdec>=-15.7])

thetamax=46.4

def ki(x):
    return (np.cos(np.deg2rad(thetamax))-np.sin(np.deg2rad(-35.15))*np.sin(x))/(np.cos(np.deg2rad(-35.15))*np.cos(x))
def funa(x):
    return np.piecewise(x,
                 [x>1,x<-1],
                 [lambda x: 0, 
                  lambda x: np.pi, 
                  lambda x: np.arccos(x)
                  ])
def expo(x):
    return(np.cos(np.deg2rad(-35))*np.cos(np.deg2rad(x))*np.sin(funa(ki(np.deg2rad(x))))
    +funa(ki(np.deg2rad(x)))*np.sin(np.deg2rad(-35))*np.sin(np.deg2rad(x)))
    