#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 11:50:00 2019

@author: Oz
"""



#del h2
h2 = Hist2D(90, -180, 180, 90, -90,25)
ids, n_stations, zeniths, azimuths, energies, timestamps, gal_lons, gal_lats = np.loadtxt("/home/Oz/LIP/Auger/auger_public.txt", unpack=True)
ra,dec=ga2equ([gal_lons,gal_lats])
ra=ra
log_energies=np.log10(energies*10**18)
ecut=(energies>=32)

h2.smoothfill(ra[ecut],dec[ecut])
h2.avgdw()

#pylab.figure()
#pylab.pcolor(*h2.data,cmap='RdYlBu_r',vmin=0,vmax=1.83)
#pylab.colorbar()

#pylab.plot(ra[(energies>ecut)],dec[(energies>ecut)],'sk', ms=1)

hrad=h2
hrad.xbins=np.deg2rad(h2.xbins)
hrad.ybins=np.deg2rad(h2.ybins)
pylab.figure();
pylab.subplot(111, projection='hammer');
pylab.pcolor(*hrad.data,cmap='RdYlBu_r', vmin=np.min(h2.hist),vmax=np.max(h2.hist));
pylab.title('E > 32 EeV\n')
pylab.colorbar()
pylab.grid()
#pylab.savefig('E_over_8_Delta_under_0.png', dpi=1000)
